package com.bdb.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bdb.app.entity.Person;

public interface PersonRepository extends JpaRepository<com.bdb.app.entity.Person, Long> {
	
	@Query("SELECT p FROM Person p WHERE p.type = :type AND p.registerStatus = 'Activo'")
	List<Person> getPersonsListByType(@Param("type") String type);
	
}
