package com.bdb.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdb.app.entity.Person;
import com.bdb.app.repository.PersonRepository;
import com.bdb.app.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {
	
	@Autowired
	PersonRepository personRepository;
	
	@Override
	public Person save(Person person) {
		return personRepository.save(person);
	}
	
	@Override
	public List<Person> getPersonsListByType(String type) {
		return personRepository.getPersonsListByType(type);
	}

	@Override
	public Person updatePerson(Person person) {
		return personRepository.save(person);
	}

	@Override
	public Person deletePerson(Long id) {
		Person person = personRepository.findById(id).orElse(null);
		person.setRegisterStatus("Inactivo");
		return personRepository.save(person);
	}

	@Override
	public List<Person> findAll() {
		return personRepository.findAll();
	}

	@Override
	public Person activePerson(Long id) {
		Person person = personRepository.findById(id).orElse(null);
		person.setRegisterStatus("Activo");
		return personRepository.save(person);
	}

}
