package com.bdb.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdb.app.entity.Doctype;

public interface DoctypeRepository extends JpaRepository<Doctype, Long> {

}
