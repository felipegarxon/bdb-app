package com.bdb.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdb.app.entity.Doctype;
import com.bdb.app.service.DoctypeService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class DoctypeController {
	
	@Autowired
	DoctypeService doctypeService;
	
	@PostMapping("/register-doctype")
	public ResponseEntity<Doctype> registerDoctype(@RequestBody Doctype doctype){
		return ResponseEntity.ok().body(doctypeService.save(doctype));
	}
	
	@GetMapping("/get-all-doctypes")
	public ResponseEntity<List<Doctype>> getAllDoctypes(){
		return ResponseEntity.ok().body(doctypeService.findAll());
	}
}
