package com.bdb.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdbappbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdbappbackendApplication.class, args);
	}

}
