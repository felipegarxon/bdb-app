package com.bdb.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdb.app.entity.Doctype;
import com.bdb.app.repository.DoctypeRepository;
import com.bdb.app.service.DoctypeService;

@Service
public class DoctypeServiceImpl implements DoctypeService {
	
	@Autowired
	DoctypeRepository doctypeRepository;
	
	@Override
	public Doctype save(Doctype doctype) {
		return doctypeRepository.save(doctype);
	}

	@Override
	public List<Doctype> findAll() {
		return doctypeRepository.findAll();
	}

}
