package com.bdb.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdb.app.entity.Person;
import com.bdb.app.service.PersonService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class PersonController {
	
	@Autowired
	PersonService personService;
	
	@PostMapping("/register-person")
	public ResponseEntity<Person> registerPerson(@RequestBody Person person){
		return ResponseEntity.ok().body(personService.save(person));
	}
	
	@GetMapping("/get-persons-list-by-type/{type}")
	public ResponseEntity<List<Person>> getPersonsListByType(@PathVariable String type){
		return ResponseEntity.ok().body(personService.getPersonsListByType(type));
	}
	
	@PutMapping("/update-person")
	public ResponseEntity<Person> updatePerson(@RequestBody Person person){
		return ResponseEntity.ok().body(personService.updatePerson(person));
	}
	
	@GetMapping("/delete-person/{id}")
	public ResponseEntity<Person> deletePerson(@PathVariable Long id){
		return ResponseEntity.ok().body(personService.deletePerson(id));
	}
	
	@GetMapping("/active-person/{id}")
	public ResponseEntity<Person> activePerson(@PathVariable Long id){
		return ResponseEntity.ok().body(personService.activePerson(id));
	}
	
	@GetMapping("/get-all-persons")
	public ResponseEntity<List<Person>> getAllPersonsList(){
		return ResponseEntity.ok().body(personService.findAll());
	}
}
