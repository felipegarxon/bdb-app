package com.bdb.app.service;

import java.util.List;

import com.bdb.app.entity.Doctype;

public interface DoctypeService {
	
	Doctype save(Doctype doctype);
	
	List<Doctype> findAll();
	
}
