package com.bdb.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person implements Serializable {

	private static final long serialVersionUID = -6925864954025959334L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "person_id")
	private Long id;
	
	@Column(name = "doctype_id")
	private int doctypeId;
	
	@Column(name = "num_doc")
	private double numDoc;
	
	private String name;
	
	@Column(name = "last_name")
	private String lastName;
	
	private String gender;
	
	private String type;
	
	private Date birth;
	
	@Column(name = "father_id")
	private Long fatherId;
	
	@Column(name = "mother_id")
	private Long motherId;
	
	@Column(name = "register_date")
	private LocalDateTime registerDate;
	
	@Column(name = "adoption_date")
	private LocalDateTime adoptionDate;
	
	@Column(name = "register_status")
	private String registerStatus;
	
	public Person() {
		this.registerDate = LocalDateTime.now();
		this.registerStatus = "Activo";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getDoctypeId() {
		return doctypeId;
	}

	public void setDoctypeId(int doctypeId) {
		this.doctypeId = doctypeId;
	}

	public double getNumDoc() {
		return numDoc;
	}

	public void setNumDoc(double numDoc) {
		this.numDoc = numDoc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public Long getFatherId() {
		return fatherId;
	}

	public void setFatherId(Long fatherId) {
		this.fatherId = fatherId;
	}

	public Long getMotherId() {
		return motherId;
	}

	public void setMotherId(Long motherId) {
		this.motherId = motherId;
	}

	public LocalDateTime getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(LocalDateTime registerDate) {
		this.registerDate = registerDate;
	}

	public LocalDateTime getAdoptionDate() {
		return adoptionDate;
	}

	public void setAdoptionDate(LocalDateTime adoptionDate) {
		this.adoptionDate = adoptionDate;
	}

	public String getRegisterStatus() {
		return registerStatus;
	}

	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}

}
