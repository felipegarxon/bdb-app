package com.bdb.app.service;

import java.util.List;

import com.bdb.app.entity.Person;

public interface PersonService {
	
	// Save person
	Person save(Person person);
	
	// Get persons list by type
	List<Person> getPersonsListByType(String type);
	
	// Update person
	Person updatePerson(Person person);
	
	// Delete person
	Person deletePerson(Long id);
	
	// Active person
	Person activePerson(Long id);
	
	// Get all persons
	List<Person> findAll();
}
