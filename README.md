#Instrucciones de ejecución#

### Base de datos - Docker - Mysql ###
- 1)  Navegue con la consola de comandos (cmd) hasta el folder ./docker
- 2)  Ejecute el comando: docker-compose up

    Al ejecutarse el comando, se creará un contenedor docker con la base de datos Mysql
    se realizará la importación de los scripts almacenados en la carpeta ./docker/mysql-dumpl
    que corresponden a la creación de tablas.

    La base de datos sera ejecutada en el puerto 3000 del localhost

### Backend (Java - SpringBoot) ###
- 3)  Una vez en ejecución la base de datos, procederemos con la ejecución de la api rest, para lo cual
      navegue con la consola de comandos hasta la carpeta ./backend/bdbappbackend/target/
- 4)  Ejecute el comando: java -jar bdbappbackend-1.0.0.jar
- 5)  Cuando el servicio este en ejecución, podrá acceder a la documentación del servicio en la url:
      localhost:8000/swagger-ui.html

### Frontend (Angular) ###
- 6)  Navegue con la consola (cmd) hasta ./frontend/app/
- 7)  Ejecute el comando: ng serve --open

      Angular subirá la aplicación en: localhost:4200
