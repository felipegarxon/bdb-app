import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { ApirestService } from '../../services/apirest.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  doctypeList;
  name:string = "";
  lastName:string = "";
  docType:number;
  numDoc:number;
  gender:string = "";
  type:string = "";
  birth:Date;

  constructor(private apirest: ApirestService) { }

  ngOnInit() {
    this.apirest.getDoctypeList()
    .subscribe({
      next:(response:any)=>{
        this.doctypeList = response;
      }
    })
  }

  registrarPersona(){

    let requestBody = {
      "birth": this.birth,
      "doctypeId": this.docType,
      "gender": this.gender,
      "lastName": this.lastName,
      "name": this.name,
      "numDoc": this.numDoc,
      "type": this.type
    }

    this.apirest.registerPerson(requestBody).subscribe({
      next:(response:any)=>{
        location.reload();
      }
    });

  }

}
