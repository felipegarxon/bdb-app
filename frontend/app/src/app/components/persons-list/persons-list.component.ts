import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApirestService } from '../../services/apirest.service';

@Component({
  selector: 'app-persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.css']
})
export class PersonsListComponent implements OnInit {

  personsList;
  colorActivo:string = "#27ae60";
  colorInactivo:string = "#c0392b";
  ninoAdoptar:any;
  idNino:number;
  father:number;
  mother:number;
  fathersList;
  mothersList;

  ninoAdoptarName:string = "";

  constructor(private apirest: ApirestService) { }

  ngOnInit(): void {
    this.listadoPersonas();
    this.listadoPadres();
  }

  listadoPersonas(){
    this.apirest.getPersonsList()
    .subscribe({
      next:(response:any)=>{
        this.personsList = response;
      }
    })
  }

  listadoPadres(){
    this.apirest.getPersonsListByType("PADRE").subscribe({
      next:(response:any)=>{
        this.fathersList = response;
      }
    });
    this.apirest.getPersonsListByType("MADRE").subscribe({
      next:(response:any)=>{
        this.mothersList = response;
      }
    });
  }

  eliminarPersona(id){
    this.apirest.deletePerson(id)
      .subscribe({
        next:(response:any)=>{
          this.listadoPersonas();
        }
      });

  }

  activarPersona(id){
    this.apirest.activePerson(id)
      .subscribe({
        next:(response:any)=>{
          this.listadoPersonas();
        }
      });
  }

  definirNinoAdoptar(person){
    this.ninoAdoptar = person;
    this.ninoAdoptarName = this.ninoAdoptar.name+" "+this.ninoAdoptar.lastName;
  }

  registrarAdopcion(){
    this.ninoAdoptar.fatherId = this.father;
    this.ninoAdoptar.motherId = this.mother;
    this.apirest.updatePerson(this.ninoAdoptar).subscribe({
      next:(response:any)=>{
          this.listadoPersonas();
      }
    });
  }

}
