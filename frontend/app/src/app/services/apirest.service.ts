import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApirestService {

  private response: any;
  private server: string;

  constructor(private http: HttpClient) {
    this.server = 'http://localhost:8000/';
  }

  public getDoctypeList(){
    return this.http.get(this.server+'get-all-doctypes');
  }

  public registerPerson(requestBody){
    return this.http.post(this.server+'register-person',requestBody);
  }

  public getPersonsList(){
    return this.http.get(this.server+'get-all-persons');
  }

  public getPersonsListByType(type){
    return this.http.get(this.server+'get-persons-list-by-type/'+type);
  }

  public deletePerson(id){
    return this.http.get(this.server+'delete-person/'+id);
  }

  public activePerson(id){
    return this.http.get(this.server+'active-person/'+id);
  }

  public updatePerson(person){
    return this.http.put(this.server+'update-person',person);
  }
}
