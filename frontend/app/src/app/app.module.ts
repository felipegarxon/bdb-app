import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './components/header-component/header-component.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { BodyComponent } from './components/body/body.component';
import { PersonsListComponent } from './components/persons-list/persons-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    RegisterFormComponent,
    BodyComponent,
    PersonsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
